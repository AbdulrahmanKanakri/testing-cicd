const express = require("express");
const fs = require("fs");

const dns = require("dns");
const axios = require("axios");

const app = express();

const PORT = 8070;

app.use(express.raw({ limit: "3mb" }));

app.get("/", async (req, res) => {
  res.send("success");
});

app.get("/check-domain", async (req, res) => {
  const { domain } = req.query;
  console.log({ domain });

  dns.resolve4(domain, (err) => {
    console.log({ err });
    res.json({
      data: { domain, available: !!err },
    });
  });
});

app.get("/nytimes", async (req, res) => {
  const searchParams = new URLSearchParams({
    // q: "election",
    sort: "newest",
    "api-key": "neEEyLdeDz0GBLvvRq2xdiymXZiqbS0u",
    // page: 0,
  });

  const url = `https://api.nytimes.com/svc/search/v2/articlesearch.json?${searchParams}`;

  const result = await axios.get(url);
  console.log({ data: result.data });
  res.json(result.data.response.docs);
});

app.post("/send", (req, res) => {
  const image = req.body;
  saveImage(image);

  res.send("success");
});

app.listen(PORT, () => console.log(`Running on http://localhost:${PORT}`));

export function saveImage(buffer) {
  const filename = `src/images/IMAGE_${new Date().getTime().toString()}.jpg`;
  fs.writeFile(filename, buffer, (err) => {
    if (err) return console.error(err);
    console.log("file saved to ", filename);
  });
}
