FROM node:18-alpine

WORKDIR /app

COPY package*.json ./

RUN npm ci

COPY . ./

EXPOSE 8070

CMD [ "node", "src/index.js" ]
